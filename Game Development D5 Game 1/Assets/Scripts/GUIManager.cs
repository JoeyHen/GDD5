﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI () {
		// Make a background box
		GUI.Box(new Rect(10,10,80,25), 
            GameManager.height + " meters");
        GUI.Box(new Rect(150, 10, 200, 25),
                                "Player 1     " + GameManager.playerWins[0] + " - " + GameManager.playerWins[1] + "     Player 2");
	}
}
